<?php
/**
 * WebConnectorTest.php
 *
 * @author: chazer
 * @created: 24.11.15 0:32
 */

namespace ISP\Manager\Tests\Connectors;

use ISP\Manager\Connectors\WebConnector;

class WebConnectorTest extends \PHPUnit_Framework_TestCase
{
    /** @var WebConnector */
    private $obj;

    protected function setUp()
    {
        $this->obj = new WebConnector();
        $this->obj->init();
    }

    protected function tearDown()
    {
    }

    public function testFunctionName()
    {
        $req = $this->obj->buildRequest('function', []);
        $this->assertContains('func=function', $req);
    }

    public function testManagerOption()
    {
        $req = $this->obj->buildRequest('none', []);
        $this->assertContains('/ispmgr?', $req);

        $req = $this->obj->buildRequest('none', [], ['m' => 'testmgr']);
        $this->assertContains('/testmgr?', $req);
    }

    public function testFormatOption()
    {
        $req = $this->obj->buildRequest('none', []);
        $this->assertContains('out=xml', $req);

        $req = $this->obj->buildRequest('none', [], ['o' => 'devel']);
        $this->assertContains('out=devel', $req);
    }

    public function testBuildRequest()
    {
        $params = ['a' => 1, 'b' => 2, 'c' => 3];
        $options = ['o' => 'devel', 'm' => 'testmgr'];

        $req = $this->obj->buildRequest('test.function', []);
        $this->assertEquals('http://localhost:1500/ispmgr?func=test.function&out=xml', $req);

        $this->obj->uri = 'http://example.com';
        $req = $this->obj->buildRequest('test.function', $params, $options);
        $this->assertEquals('http://example.com/testmgr?a=1&b=2&c=3&func=test.function&out=devel', $req);
    }
}
