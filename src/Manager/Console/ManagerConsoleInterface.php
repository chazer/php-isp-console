<?php
/**
 * ManagerConsoleInterface.php
 *
 * @author: chazer
 * @created: 09.03.16 13:57
 */

namespace ISP\Manager\Console;

use ISP\Manager\Interfaces\IManagerConsole;

interface ManagerConsoleInterface extends IManagerConsole
{
}
