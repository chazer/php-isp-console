<?php
/**
 * ManagerConsoleAwareInterface.php
 *
 * @author: chazer
 * @created: 29.02.16 21:03
 */

namespace ISP\Manager\Console;

interface ManagerConsoleAwareInterface
{
    /**
     * @param ManagerConsoleInterface $console
     */
    public function setConsole(ManagerConsoleInterface $console);
}
