<?php
/**
 * ManagerConsoleAwareTrait.php
 *
 * @author: chazer
 * @created: 29.02.16 21:03
 */

namespace ISP\Manager\Console;

trait ManagerConsoleAwareTrait
{
    /** @var ManagerConsoleInterface */
    protected $console;

    /**
     * @param ManagerConsoleInterface $console
     */
    public function setConsole(ManagerConsoleInterface $console)
    {
        $this->console = $console;
    }
}
