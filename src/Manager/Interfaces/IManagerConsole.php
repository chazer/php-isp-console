<?php
/**
 * IManagerConsole.php
 *
 * @author: chazer
 * @created: 23.11.15 17:56
 */

namespace ISP\Manager\Interfaces;

interface IManagerConsole
{
    /**
     * Call manager function
     *
     * @param string $func function name
     * @param mixed $elid
     * @param array $params
     * @param mixed $auth
     * @param string $outType
     * @return mixed
     * @deprecated
     */
    public function runcmd($func, $elid = false, array $params = [], $auth = false, $outType = 'json');

    /**
     * Call manager function
     *
     * @param string $func function name
     * @param array $params
     * @param array $options
     * @return mixed
     */
    public function call($func, $params = [], $options = []);
}
