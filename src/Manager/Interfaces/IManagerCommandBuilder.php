<?php
/**
 * IManagerCommandBuilder.php
 *
 * @author: chazer
 * @created: 24.11.15 1:45
 */

namespace ISP\Manager\Interfaces;

interface IManagerCommandBuilder extends IManagerConsole
{
    /**
     * @param string $name
     * @param null|string $value
     * @return $this
     */
    public function setOption($name, $value);

    /**
     * @param string $name
     * @return null|string
     */
    public function getOption($name);

    /**
     * @param string $name
     * @param null|string $value
     * @return $this
     */
    public function setParam($name, $value);

    /**
     * @param string $name
     * @return null|string
     */
    public function getParam($name);
}
