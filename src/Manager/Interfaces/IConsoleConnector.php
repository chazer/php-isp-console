<?php
/**
 * IConsoleTransport.php
 *
 * @author: chazer
 * @created: 23.11.15 17:58
 */

namespace ISP\Manager\Interfaces;

interface IConsoleConnector
{
    /**
     * @throw Exception
     */
    public function init();

    /**
     * @param string $func function name
     * @param array $params
     * @param null|array $options set to null for use default options
     * @return mixed
     */
    public function buildRequest($func, $params, $options = null);

    /**
     * @param $command
     * @return mixed
     */
    public function execute($command);
}
