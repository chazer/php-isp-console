<?php
/**
 * Console.php
 *
 * @author: chazer
 * @created: 23.11.15 18:02
 */

namespace ISP\Manager;

use ISP\Manager\ConnectorAware;
use ISP\Manager\Console\ManagerConsoleInterface;
use ISP\Manager\Interfaces\IManagerCommandBuilder;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class Console implements IManagerCommandBuilder, LoggerAwareInterface, ManagerConsoleInterface
{
    use LoggerAwareTrait;
    use ConnectorAware;

    protected $options = [
        // ISP module
        'm' => 'ispmgr',
        // Output format
        'o' => 'xml',
    ];

    protected $params = [];

    public $decodeAsJson = ['json', 'old_json'];

    public function setOption($name, $value)
    {
        $this->options[$name] = $value;
        return $this;
    }

    public function getOption($name)
    {
        return isset($this->options[$name])
            ? $this->options[$name]
            : null;
    }

    public function setParam($name, $value)
    {
        $this->params[$name] = (string) $value;
        if ($name === null) {
            unset($this->params[$name]);
        }
        return $this;
    }

    public function getParam($name)
    {
        return isset($this->params[$name])
            ? $this->params[$name]
            : null;
    }

    /**
     * @return null|string
     */
    public function getManager()
    {
        return $this->getOption('m');
    }

    /**
     * @param null|string $manager
     * @return $this
     */
    public function setManager($manager)
    {
        return $this->setOption('m', $manager);
    }

    /**
     * @return null|string
     */
    public function getFormat()
    {
        return $this->getOption('o');
    }

    /**
     * @param null|string $format
     * @return $this
     */
    public function setFormat($format)
    {
        return $this->setOption('o', $format);
    }

    public function runcmd($func, $elid = false, array $params = [], $auth = false, $outType = 'json')
    {
        $options = $this->options;
        $auth && $params['auth'] = $auth;
        $elid && $params['elid'] = $elid;
        $outType && $options['o'] = $outType;
        $command = $this->formatCmd($func, $params, $options);
        if (false === $command)
            return false;

        $out = $this->execute($command);
        if (in_array($options['o'], $this->decodeAsJson)) {
            return json_decode($out);
        }
        return $out;
    }

    public function call($func, $params = [], $options = [])
    {
        $options = array_merge($this->options, $options);
        $command = $this->formatCmd($func, $params, $options);
        if (false === $command)
            return false;

        $out = $this->execute($command);
        if (in_array($options['o'], $this->decodeAsJson)) {
            return json_decode($out);
        }
        return $out;
    }

    /**
     * @param string $func function name
     * @param array $params
     * @param null|array $options set to null for use default options
     * @return mixed command
     */
    protected function formatCmd($func, $params, $options = null)
    {
        $options = is_null($options) ? $this->options : $options;
        return $this->getConnector()->buildRequest($func, $params, $options);
    }

    /**
     * @param string $command
     * @return string
     */
    protected function execute($command)
    {
        $this->logger && $this->logger->info('Execute command: ' . var_export($command, true));
        $out = $this->getConnector()->execute($command);
        $this->logger && $this->logger->info('Command return: ' . var_export($out, true));
        return $out;
    }
}
