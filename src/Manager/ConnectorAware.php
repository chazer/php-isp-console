<?php
/**
 * ConnectorAware.php
 *
 * @author: chazer
 * @created: 23.11.15 18:48
 */

namespace ISP\Manager;

use ISP\Manager\Interfaces\IConsoleConnector;

trait ConnectorAware
{
    /** @var IConsoleConnector */
    private $_connector;

    /**
     * @param IConsoleConnector $connector
     */
    public function setConnector($connector)
    {
        $this->_connector = $connector;
    }

    /**
     * @return IConsoleConnector
     */
    public function getConnector()
    {
        return $this->_connector;
    }
}
