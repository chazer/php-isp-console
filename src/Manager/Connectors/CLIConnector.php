<?php
/**
 * CLIConnector.php
 *
 * @author: chazer
 * @created: 23.11.15 18:23
 */

namespace ISP\Manager\Connectors;

use Exception;
use ISP\Manager\Interfaces\IConsoleConnector;

class CLIConnector implements IConsoleConnector
{
    /**
     * @var false|string path to mgrctl executable file
     */
    public $mgrctl = '/usr/local/ispmgr/sbin/mgrctl';

    public $defaultOptions = [
        'm' => 'ispmgr',
        'o' => 'xml',
    ];

    public function init()
    {
        if (false !== $this->mgrctl) {
            if (!is_file($this->mgrctl))
                throw new Exception("Couldn't find mgrctl tool");

            if (!is_executable($this->mgrctl) && (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN'))
                throw new Exception("This file must be executable: " . $this->mgrctl);
        }
    }

    /**
     * @param string $func function name
     * @param array $params
     * @param null|array $options set to null for use default options
     * @return false|string command line
     */
    public function buildRequest($func, $params, $options = null)
    {
        if (!$this->mgrctl)
            return false;

        $options = is_null($options) ? [] : $options;
        $options = array_merge($this->defaultOptions, $options);

        $a = [$this->mgrctl];
        foreach ($options as $key => &$v)
            $a[] = sprintf('-%s %s', $key, escapeshellarg($v));

        $a[] = $func;

        foreach ($params as $key => &$v)
            $a[] = sprintf('%s=%s', $key, escapeshellarg($v));

        return implode(' ', $a);
    }

    /**
     * @param mixed $command
     * @return string
     */
    public function execute($command)
    {
        $out = shell_exec($command);
        return $out;
    }
}
